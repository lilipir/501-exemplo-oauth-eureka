package com.poc.zuul;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
public class LoginFilter extends ZuulFilter {
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Override
  public boolean shouldFilter() {
    RequestContext ctx = RequestContext.getCurrentContext();
    return ctx.getRequest().getRequestURI().contains("oauth/token");
  }

  @Override
  public Object run() throws ZuulException {
    RequestContext ctx = RequestContext.getCurrentContext();
    
    byte[] encoded;
    
    try {
      encoded = Base64.getEncoder().encode("zuul:zuul123".getBytes("UTF-8"));
      ctx.addZuulRequestHeader("Authorization", "Basic " + new String(encoded));
    } catch (UnsupportedEncodingException e) {
      logger.error("Error occured in pre filter", e);
    }
    
    return null;
  }

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return -2;
  }

}
