package com.poc.foto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FotoController {
  @Autowired
  FotoRepository repository;
  
  @PostMapping("/foto")
  public Foto criar(@RequestBody Foto foto, @AuthenticationPrincipal Usuario usuario) {
    foto.setNomeUsuario(usuario.getNome());
    foto.setIdUsuario(usuario.getId());
    
    return repository.save(foto);
  }
  
  @GetMapping
  public Iterable<Foto> listar(){
    return repository.findAll();
  }
}
